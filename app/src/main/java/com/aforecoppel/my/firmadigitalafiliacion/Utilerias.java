package com.aforecoppel.my.firmadigitalafiliacion;

import android.app.Activity;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

@SuppressWarnings("ALL")
class Utilerias {

    //ARCHIVO CONFIGURACIÓN
    static final String CONFIG_KEY1 = "encoioui";

    //RUTAS CARPETAS
    static final String DIR_EXTERNA = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    static final String DIR_REENROL = "Afiliacion/";
    static final String DIR_FORMATO = "FormatoEnrolamiento/";
    static final String DIR_DIGITAL = "Digitalizador/";
    static final String DIR_TEMP    = "Temp/";
    static final String DIR_HUELLAS = "Huellas/";
    static final String DIR_LOGS    = "Log/";

    static final String LOG_REENROL = DIR_EXTERNA + DIR_REENROL + DIR_LOGS + "logReenrolMovil";

    static final int PERMISSION_ALL = 50;
    static final String[] PERMISSIONS = {
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
    };

    //MANEJO DE HUELLAS
    static final int HUELLAS_AMBAS          = 1;
    static final int HUELLAS_IZQ_FALTA      = 2;
    static final int HUELLAS_DER_FALTA      = 3;
    static final int HUELLAS_VERIFICACION   = 4;
    static final String HUELLAS_IZQ_LESION     = "006";
    static final String HUELLAS_IZQ_NOTIENE    = "003";
    static final String HUELLAS_DER_LESION     = "005";
    static final String HUELLAS_DER_NOTIENE    = "002";

    //IDENTIFICADORES PARA LA LLAMADA AL SERVICIO wsafiliacionmovil
    static final int SOAP_TEMPLATES_EMPLEADO    = 110;
    static final int SOAP_VALIDAR_EMPLEADO      = 111;
    static final int SOAP_VALIDAR_TRABAJADOR    = 120; //fnvalidarreenroltrabajador
    static final int SOAP_OBT_FOLIO_SERVICIO    = 121; //fnobtenfolioservicio
    static final int SOAP_CANCELAR_REENROL      = 122; //fnactualizarfolioreenrolamiento
    static final int SOAP_REINICIAR_REENROL     = 123;
    static final int SOAP_FOLIO_ENROLAMIENTO    = 124;
    static final int SOAP_GUARDAR_HUELLAS       = 125;
    static final int SOAP_GEN_FORMATO_REENROL   = 126;
    static final int SOAP_GUARDAR_FIRMA_ENROL   = 127;
    static final int SOAP_MOVER_IMG_FIRMA       = 128;
    static final int SOAP_OBTEN_ESTATUS_ENROL   = 129; //fnObtenerEstatusEnrolamiento
    static final String IDENT_IP        = "Identificador";
    // IP PARA EL WS
    static final String IP = "http://10.27.142.196:50131";

    //LLAMADAS A OTRAS ACTIVIDADES
    static final int  LLAMAR_REENROLAMIENTO      = 300;
    static final int  LLAMAR_LECTOR_HUELLAS      = 301;
    static final int  LLAMAR_DIALOGO_EXCEP       = 302;
    static final int  LLAMAR_FORMATO_ENROL       = 303;
    static final int  LLAMAR_FIRMA_ENROLA        = 304;
    static final int  LLAMAR_DIGITALIZADOR       = 305;

    //NOMBRES APLICACIONES NECESARIAS
    static final String NAME_REENROLAMIENTO = "Reenrolamiento Móvil";
    static final String NAME_LECTOR_HUELLAS = "EnrollMovil";
    static final String NAME_FIRMA_ENROLA   = "Firma Digital";
    static final String NAME_DIGITALIZADOR  = "Digitalizador Móvil";

    //HOST Y METODOS APIREST
    static final String REST_EMPL = "https://bioafoemployee.coppel.io";
    static final String REST_CLIE = "https://bioafocustomer.coppel.io";
    static final String REST_IMGE = "/bio/imgenroll";  //CREAR TEMPLATES
    static final String REST_TENF = "/bio/tenfingers"; //COMPARAR TEMPLATES
    //RESPUESTAS APIREST
    static final String REST_RP_EST = "Status";
    static final String REST_RP_MEN = "Message";
    static final String REST_RP_RES = "Resultado";
    static final String REST_RP_FRT = "FingerTemplate";

    //CONSTANTES PARA LLAMADOS DE ACTIVITIES
    //---LECTOR_HUELLAS_ACTIVITY
    static final String ACT_HUELLAS_PKG = "com.example.leonardofigueroa.enrolmovil";
    static final String ACT_HUELLAS_CLS = "com.example.leonardofigueroa.enrolmovil.MainActivity";
    static final String ACT_HUELLAS_OPC = "IN_1"; //1=AMBAS - 2=DERECHA - 3=IZQUIERDA - 4=VERIFICACIÓN
    static final String ACT_HUELLAS_NUM = "IN_2";
    static final String ACT_HUELLAS_IMG = "IN_3"; //0=NO - 1=SI
    static final String ACT_HUELLAS_OT1 = "OUT_1"; //CODIGO DE RESPUESTA
    static final String ACT_HUELLAS_OT2 = "OUT_2"; //JSON
    static final String ACT_HUELLAS_OT3 = "OUT_3"; //NO. SERIE
    static final String ACT_HUELLAS_OT4 = "OUT_4"; //RUTA ARCHIVOS

    //PARAMETRO PARA LAS APPS QUE CONTIENE EL SERVIDOR AL QUE SE CONECTARÁ
    static final String PARAM_SERVIDOR  = "encoioui";//String

    //---LLAMAR_REENROLAMIENTO
    static final String ACT_REENROL_PKG = "com.aforecoppel.my.reenrolamientomovil";
    static final String ACT_REENROL_CLS = "com.aforecoppel.my.reenrolamientomovil.ReenrolActivity";
    static final String ACT_REENROL_NUM = "empleado";
    static final String ACT_REENROL_NOM = "nombre";
    static final String ACT_REENROL_CEN = "centro";

    //---MOSTRAR_FORMATO_ENROL
    static final String ACT_FORMATO_PDF   = "namePDF";
    static final String ACT_FORMATO_FOAFI = "sFolioAfiliacion";
    static final String ACT_FORMATO_FOSER = "sFolioServicio";
    static final String ACT_FORMATO_FIRMA = "tipoFirma";

    //---LLAMAR_FIRMA
    static final String ACT_FIRMA_PKG = "com.aforecoppel.my.firmadigital";
    static final String ACT_FIRMA_CLS = "com.aforecoppel.my.firmadigital.SignActivity";
    static final String ACT_FIRMA_FOAFI = "sFolioAfiliacion";
    static final String ACT_FIRMA_FOSER = "sFolioServicio";
    static final String ACT_FIRMA_TIPOF = "tipoFirma";
    static final String ACT_FIRMA_NOMFIR= "nombreFirma";
    static final String ACT_FIRMA_PERSONAF = "personaFirma";

    //---LLAMAR_DIGITALIZADOR
    static final String ACT_DIGI_PKG = "com.aforecoppel.my.digitalizadormovil";
    static final String ACT_DIGI_CLS = "com.aforecoppel.my.digitalizadormovil.MainListActivity";
    static final String ACT_DIGI_TIPO   = "tipoOperacion";  //int
    static final String ACT_DIGI_TABLA  = "tablaConsulta";  //int
    static final String ACT_DIGI_FOAFI  = "folioAfiliacion";//long
    static final String ACT_DIGI_FOSER  = "folioServicio";  //long
    static final String ACT_DIGI_NOEMP  = "numEmp";         //long

    //VALORES DE RETORNO
    static final String ERROR_CODE_RESPONSE     = "ErrorCode";
    static final int ERROR_RESPONSE_EMPTY_PARAMS    = 800;
    static final int ERROR_RESPONSE_NO_PERMISSIONS  = 810;
    static final int ERROR_RESPONSE_BTN_CANCEL      = 820;
    static final int ERROR_RESPONSE_WRONG_STATUS    = 830;
    static final int ERROR_RESPONSE_WS_FAILED       = 840;
    static final int ERROR_RESPONSE_DIR_NO_EXISTE   = 850;

    //PATRÓN PARA EL CURP
    private static final String regex =
            "[A-Z][AEIOU][A-Z]{2}[0-9]{2}" +
                    "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
                    "[HM]" +
                    "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
                    "[B-DF-HJ-NP-TV-Z]{3}" +
                    "[0-9A-Z][0-9]$";

    static Pattern patron = Pattern.compile(regex);

    static void showToast(Activity act, String men){
        Toast.makeText(act, men, Toast.LENGTH_LONG).show();
    }

    static void showMessage(AppCompatActivity act, MessageDialog message){
        message.setCancelable(false);
        message.show(act.getSupportFragmentManager(), message.getTag());
    }

    static void cargando(ConstraintLayout bar, boolean flag){
        //TRUE = VISIBLE | FALSE = GONE
        bar.setVisibility(flag ? View.VISIBLE : View.GONE);
    }

    static boolean writeFile(File file, byte[] content, boolean append){
        try {
            FileOutputStream fos = new FileOutputStream(file, append);
            fos.write(content);
            fos.flush();
            fos.close();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    static void writeLog(String strLog){

        String todayTime = new SimpleDateFormat("'|'dd-MM-yy'|'HH:mm:ss'|'").format(new Date());
        String todayLog = new SimpleDateFormat("dd-MM-yy").format(new Date());

        strLog = todayTime + strLog + "\n";
        File log = new File(LOG_REENROL + todayLog + ".txt");

        writeFile(log, strLog.getBytes(), true);
    }

    static String readFile(String filePath) {
        String s;
        StringBuilder fileContent = new StringBuilder();

        try {
            File myFile = new File(filePath);
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(
                    new InputStreamReader(fIn));

            while ((s = myReader.readLine()) != null) {
                fileContent.append(s).append("\n");
            }
            myReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContent.toString();
    }

    static void mkDirs(String DIR){
        new File(DIR).mkdirs();
    }

    static HashMap<String, String> getValuesXML(String xmlMen){

        HashMap<String, String> values = new HashMap<>();

        try {
            DocumentBuilder newDocumentBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document parse = newDocumentBuilder.parse(
                    new ByteArrayInputStream(xmlMen.getBytes()));

            //LA ETIQUETA OutParam CONTIENE LA INFORMACIÓN QUE REGRESA EL SERVICIO
            NodeList node = parse.getElementsByTagName("outParam");
            Node elem = node.item(0);

            Node child;
            String name;

            if(elem != null){
                if(elem.hasChildNodes()){
                    for( child = elem.getFirstChild(); child != null; child = child.getNextSibling() ){
                        name = child.getNodeName();
                        switch (name) {
                            case "EstadoProc": {
                                Node n = parse.getElementsByTagName("EstadoProc").item(0);
                                values.put("Estatus", n.getFirstChild().getTextContent());
                                values.put("EsMensaje", n.getFirstChild().getNextSibling().getTextContent());
                                break;
                            }
                            case "TemplatesProm": {
                                Node n = parse.getElementsByTagName("TemplatesProm").item(0);

                                Node hijos;
                                for (hijos = n.getFirstChild(); hijos != null; hijos = hijos.getNextSibling()) {

                                    String finger = hijos.getFirstChild().getNodeName();
                                    String dedo = hijos.getFirstChild().getTextContent();

                                    String template = hijos.getFirstChild().getNextSibling().getTextContent();

                                    values.put(finger + dedo, template);
                                }
                                break;
                            }
                            default:
                                values.put(child.getNodeName(), child.getTextContent());
                                break;
                        }
                    }
                }
            }

            return values;

        }catch(IOException e){
            e.getStackTrace();
            return null;
        } catch(ParserConfigurationException e){
            e.getStackTrace();
            return null;
        } catch(SAXException e){
            e.getStackTrace();
            return null;
        }
    }

    static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    String hex = Integer.toHexString(b & 0xFF);
                    if (hex.length() == 1)
                        hex = "0".concat(hex);
                    res1.append(hex).append(":");
                }

                res1.deleteCharAt(res1.length()-1);

                return res1.toString();
            }
        } catch (Exception ex) {
            writeLog("Error en getMacAddr" + ex.getMessage());
            ex.printStackTrace();
        }
        return "";
    }

    static String getTokenAccess(String apikey, boolean isTokenAccess){
        String hashHex = "";
        String cadena = "";
        String mac = getMacAddr();  //Obtiene la mac del dispositivo.
        String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date()); //'2018-10-29'

        if(isTokenAccess) {
            if (!mac.equals(""))
                cadena = apikey + todayDate + mac;
            else
                writeLog("Ocurrió un problema al generar el TokenAccess");
        }
        else
            cadena = apikey + todayDate;

        hashHex = bin2hex(getHash(cadena));

        return hashHex;
    }

    private static byte[] getHash(String password) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "x", new BigInteger(1, data));
    }
}