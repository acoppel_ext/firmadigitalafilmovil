package com.aforecoppel.my.firmadigitalafiliacion;

public interface AsyncTaskCompleteListener<T> {

    void onTaskComplete(T result, int id);

}
