package com.aforecoppel.my.firmadigitalafiliacion;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.simplify.ink.InkView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import static com.aforecoppel.my.firmadigitalafiliacion.Utilerias.*;

public class SignActivity extends AppCompatActivity implements AsyncTaskCompleteListener<String>,
        View.OnClickListener{

    private String DIR_ACTUAL = DIR_EXTERNA + DIR_REENROL;

    //Variables para la petición y chequeo de permisos
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private InkView ink;
    private LinearLayout ly_ink;
    private FloatingActionsMenu fab;
    private TextView tvVersionF;
    private FloatingActionButton btnFabSave;
    private FloatingActionButton btnFabClear;
    private FloatingActionButton btnFabCancel;
    private TextView mTexto;

    private int folioAfi; //Folio Afiliacion
    private int folioSer; //Folio Servicio
    private String sFirma; //TipoFirma = A quien corresponde la firma/nombre
    private String sPersona=""; //1 = Trabajador , 2= Promotor
    private String nombreFirma;
    private String ipAddress;

    private String MensajeLog = "";
    private String EstatusLog = "Firmadigital - SignActivity Version_name:"+BuildConfig.VERSION_NAME+" Version_code:"+BuildConfig.VERSION_CODE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyStoragePermissions(this);
        setContentView(R.layout.activity_sign);

        setComponentes();
        setListeners();

        //TOMA EL VALOR DEL URI QUE LLAMO LA APLICACIÓN
        Intent intent = getIntent();
        getParameters(intent);

        cambiarTexto();

        new CallWebService(ipAddress);

        getDimension();
        tvVersionF.setText("V "+BuildConfig.VERSION_NAME);
    }

    @Override //SE CANCELA CUALQUIER ACCIÓN CON EL BOTÓN DE REGRESAR.
    public void onBackPressed() {

        //finalizarActividad();
    }

    private void setComponentes(){
        //Inicializar zona para la firma y seleccionar color y grosor
        ink = (InkView) findViewById(R.id.ink);
        ink.setColor(getResources().getColor(android.R.color.black));
        ink.setMinStrokeWidth(0.1f);
        ink.setMaxStrokeWidth(3f);


        ly_ink = (LinearLayout) findViewById(R.id.layout_ink);

        //Inicializar los botones de la interfaz
        fab = (FloatingActionsMenu) findViewById(R.id.btn_fabMenu);

        btnFabSave   = (FloatingActionButton) findViewById(R.id.btn_fabSave);
        btnFabClear  = (FloatingActionButton) findViewById(R.id.btn_fabClear);
        btnFabCancel = (FloatingActionButton) findViewById(R.id.btn_fabCancel);
        tvVersionF   = (TextView) findViewById(R.id.tvVersinoF);
        mTexto = (TextView) findViewById(R.id.txt_firma_titulo);

        btnFabSave.setIcon(R.drawable.ic_save);
        btnFabClear.setIcon(R.drawable.ic_redo);
        btnFabCancel.setIcon(R.drawable.ic_cancel);
    }

    private void setListeners(){
        btnFabSave.setOnClickListener(this);
        btnFabClear.setOnClickListener(this);
        btnFabCancel.setOnClickListener(this);
    }

    private void cambiarTexto(){
        String instruc = "";

        switch(sPersona) {
            case "1": {
                instruc = getString(R.string.firma_trabajador);
                break;
            }
            case "2": {
                instruc = getString(R.string.firma_promotor);
                break;
            }
            default: {
                instruc = getString(R.string.please_sign_above);
                break;
            }
        }

        mTexto.setText(instruc);
    }

    private void getParameters(Intent intent){
        //TOMA LOS VALORES DE LOS PARAMETROS
        MessageDialog aviso;

        Bundle extra = intent.getExtras();
        if(extra == null){
            aviso = new MessageDialog();
            aviso.setMensaje("Parámetros incorrectos");
            aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finalizarActividad());
            showMessage(this, aviso);
        }else{
            try{
                ipAddress = extra.getString(CONFIG_KEY1);
                Log.e("ADDRESS", ipAddress);
                folioAfi = extra.getInt(ACT_FIRMA_FOAFI, 0);
                folioSer = extra.getInt(ACT_FIRMA_FOSER, 0);
                sFirma = extra.getString(ACT_FIRMA_TIPOF);
                sPersona = extra.getString(ACT_FIRMA_PERSONAF);
                DIR_ACTUAL += folioAfi + "/" + DIR_FORMATO;
            }
            catch (NullPointerException e){
                aviso = new MessageDialog();
                aviso.setMensaje("Error al obtener los parámetros");
                aviso.setPositiveButton("Aceptar", (dialogInterface, i) -> finalizarActividad());
                showMessage(this, aviso);
            }
        }

        /*ipAddress = "http://10.27.142.196:50131";;
        Log.e("ADDRESS", ipAddress);
        folioAfi = 16171289;
        folioSer = 0;
        sFirma = "firma";

        DIR_ACTUAL += folioAfi + "/" + DIR_FORMATO;*/
    }

    private void getDimension(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            display.getRealSize(size);
        } catch (NoSuchMethodError err) {
            display.getSize(size);
        }
        //int width = size.x;
        int width = 2000;
        double scale = (double)width/2000;

        int newHe = (int)(700 * scale);

        LayoutParams lp = ly_ink.getLayoutParams();
        lp.width = width;
        lp.height =  newHe;
        ly_ink.setLayoutParams(lp);
    }

    //EVENTO CLICK PARA LOS BOTONES DE LA INTERFAZ
    @Override
    public void onClick(View view) {

        if(view.getId() == btnFabSave.getId()){

            fab.collapse();

            Bitmap signatureBitmap = ink.getBitmap();

            nombreFirma = "FARE_" + (folioSer != 0 ? folioSer + "-S" : folioAfi) + "_" + sFirma + ".JPG";
            String pathName = DIR_ACTUAL + nombreFirma;

            if (addJpgSignatureToGallery(signatureBitmap, pathName)) {
                soapSubirFirma(pathName);
            } else {
                showToast(this, "Ocurrio un error al guardar la imagen, favor de intentarlo de nuevo");
            }
        }
        else if(view.getId() == btnFabClear.getId()){
            ink.clear();
            fab.collapse();
        }
        else if(view.getId() == btnFabCancel.getId()){
            finalizarActividad();
        }
    }

    @Override
    public void onTaskComplete(String xmlMen, int id){

        //MessageDialog aviso;

        if(xmlMen.equals("")){
            showToast(this,
                    "Promotor, ocurrió un problema durante el proceso, favor de volver a intentar.");
            return;
        }
        HashMap<String, String> values = getValuesXML(xmlMen);
        writeLog(values.toString());
        if(values == null){
            showToast(this, "Error al procesar la respuesta del servicio");
            return;
        }

        int iEstatus = Integer.parseInt(values.get("Estatus"));
        //String esMensaje = values.get("EsMensaje");

        if(iEstatus != 200 && iEstatus != 201){
            showToast(this, "Ocurrio un error con el servidor");
            return;
        }

        //SE VALIDA DE CUAL LLAMADO VUELVE EL WEBSERVICE
        switch (id) {

            case SOAP_GUARDAR_FIRMA_ENROL:


                if(iEstatus == 200){
                    Intent respuesta = new Intent();
                    Bundle extra = new Bundle();
                    extra.putString(ACT_FIRMA_NOMFIR, nombreFirma);
                    respuesta.putExtras(extra);
                    setResult(Activity.RESULT_OK, respuesta);
                    finish();
                }
                else
                    showToast(this, "Promotor, ocurrió un problema al subir la imagen, " +
                            "favor de intentarlo de nuevo");

                break;
        }
    }

    public boolean addJpgSignatureToGallery(Bitmap signature, String pathName) {

        boolean result = false;

        try {
            File photo = new File(pathName);

            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);

            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void soapSubirFirma(String pathName){

        Bitmap bm = BitmapFactory.decodeFile(pathName);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "SubirImagenesDigitalizadas");
        sSoap.setParams("cNombreImagen", nombreFirma);
        sSoap.setParams("sImagen", encodedImage);
        sSoap.setParams("cVersion",EstatusLog);

        CallWebService callWS = new CallWebService(this,
                SOAP_GUARDAR_FIRMA_ENROL);
        callWS.setFlag(true);
        callWS.execute(sSoap.getSoapString());
    }

    private void soapGuardarLogs(){
        new CallWebService(ipAddress);
        SoapString sSoap = new SoapString("wsafiliacionmovil",
                "GuardarLogs");

        sSoap.setParams("Mensaje", MensajeLog);
        sSoap.setParams("Estatus", EstatusLog);

        CallWebService callWS = new CallWebService(this,0);
        callWS.setFlag(false);
        callWS.execute(sSoap.getSoapString());
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        SignActivity.this.sendBroadcast(mediaScanIntent);
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Log.e("HEIGHT:", bitmap.getHeight() + "");
        Log.e("WIDTH:", bitmap.getWidth() + "");

        Canvas canvas = new Canvas(newBitmap);

        canvas.drawColor(Color.WHITE);

        canvas.drawBitmap(bitmap, 0, 0, null);

        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        stream.close();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(SignActivity.this, "No se pueden guardar imagenes en el almacenamiento externo", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // CHECA SI CONTAMOS CON EL PERMISO DE ESCRITURA
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // COMO NO HAY PERMISO, MUESTRA AL USUARIO LA SOLICITUD DEL PERMISO
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    //Evento que permite cerrar el menú cuando se da click en algún lugar de la pantalla
    @Override
    public boolean dispatchTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN){

            if(fab.isExpanded()){

                Rect outRect = new Rect();
                fab.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int) event.getRawX(), (int) event.getRawY()))
                    fab.collapse();
            }
        }

        return super.dispatchTouchEvent(event);
    }

    public void finalizarActividad(){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}

